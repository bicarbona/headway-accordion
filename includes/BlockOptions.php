<?php

    class headway_accordionBlockOptions extends HeadwayBlockOptionsAPI {
	
	
	public $tabs = array(
		'featured-setup' => 'Features Setup',
		'query-filters' => 'Query Filters',

	);

	public $inputs = array(
		'featured-setup' => array(
		// // Heading
		// 'navigation-heading'	=> array(
		// 	'name'	 => 'navigation-heading',
		// 	'type'	 => 'heading',
		// 	'label'	 => 'Navigation'
		// ),
			

		// 	'prev-nav'	=> array(
		// 		'name'	 => 'prev-nav',
		// 		'type'	 => 'select',
		// 		'label'	 => 'Navigation Prev',
		// 		'default' => 'ss-navigateleft',
		// 		'options' => array(
		// 			'ss-navigateleft' => 'ss-navigateleft',
		// 			'fa fa-caret-left' => 'fa fa-caret-left',


		// 	 		//'right' => 'right'
		// 		),
		// 	//	'tooltip' => 'tooltip'
		// 	),

		// 	// 'prev-nav'	=> array(
		// 	// 	'name'	 => 'prev-nav',
		// 	// 	'type'	 => 'select',
		// 	// 	'label'	 => '< Prev',
		// 	// 	'tooltip' => 'tooltip'
		// 	// ),
		// 	// 'next-nav'	=> array(
		// 	// 	'name'	 => 'next-nav',
		// 	// 	'type'	 => 'text',
		// 	// 	'label'	 => 'Next >',
		// 	// 	'tooltip' => 'tooltip'
		// 	// ),

		// 	'next-nav'	=> array(
		// 		'name'	 => 'next-nav',
		// 		'type'	 => 'select',
		// 		'label'	 => 'Navigation Next',
		// 		'default' => 'ss-navigateright',
		// 		'options' => array(
		// 			'ss-navigateright' => 'ss-navigateright',
		// 			'fa fa-caret-right' => 'fa fa-caret-right',			 		//'right' => 'right'
		// 		),
		// 	//	'tooltip' => 'tooltip'
		// 	),



		// Heading
		'heading-title'	=> array(
			'name'	 => 'heading-title',
			'type'	 => 'heading',
			'label'	 => 'Title'
		),
			'title'	=> array(
				'name'	 => 'title',
				'type'	 => 'text',
				'label'	 => 'Title',
				//'tooltip' => 'tooltip'
			),

			'title-html-tag' => array(
				'type' => 'select',
				'name' => 'title-html-tag',
				'label' => 'Title HTML tag',
				'default' => 'h1',
				'options' => array(
					'h1' => '&lt;H1&gt;',
					'h2' => '&lt;H2&gt;',
					'h3' => '&lt;H3&gt;',
					'h4' => '&lt;H4&gt;',
					'h5' => '&lt;H5&gt;',
					'h6' => '&lt;H6&gt;',
					'span' => '&lt;span&gt;'
				)
			),

			'title-link' => array(
				'type' => 'checkbox',
				'name' => 'title-link',
				'label' => 'Link Title?'
			),

			
		// Heading
		'debug-heading'	=> array(
			'name'	 => 'debug-heading',
			'type'	 => 'heading',
			'label'	 => 'Debug Info'
		),
			
			'debug' => array(
				'type' => 'checkbox',
				'name' => 'debug', 
				'label' => 'Show: DEBUG',
				'default' => false,
				'tooltip' => 'Show a . ',
			),

		),

		'query-filters' => array(
			
			// 'paginate' => array(
			// 	'type' => 'checkbox',
			// 	'name' => 'paginate',
			// 	'label' => 'Paginate Featuress',
			// 	'tooltip' => '',
			// 	'default' => true,
			// 	'tooltip' => 'Enabling pagination adds buttons to the bottom of the pin board to go to the next/previous page.  <strong>Note:</strong> If infinite scrolling is enabled, pagination will be hidden. '
			// ),
			'total' => array(
				'type' => 'slider',
				'name' => 'total',
				'label' => 'Total',
				'slider-min' => 3,
				'slider-max' => 24,
				'slider-interval' => 1,
				'tooltip' => '',
				'default' => 10,
				'tooltip' => 'How many posts to show per row. ',
				'callback' => ''
			),
			'categories' => array(
				'type' => 'multi-select',
				'name' => 'categories',
				'label' => 'Categories',
				'tooltip' => '',
				'options' => 'get_categories()',
				'tooltip' => 'Filter the pins that are shown by categories. '
			),
			
			'categories-mode' => array(
				'type' => 'select',
				'name' => 'categories-mode',
				'label' => 'Categories Mode',
				'tooltip' => '',
				'options' => array(
					'include' => 'Include',
					'exclude' => 'Exclude'
				),
				'tooltip' => 'If this is set to <em>include</em>, then only the pins that match the categories filter will be shown.  If set to <em>exclude</em>, all pins that match the selected categories will not be shown. '
			),
			
			'post-type' => array(
				'type' => 'select',
				'name' => 'post-type',
				'label' => 'Post Type',
				'tooltip' => '',
				'options' => 'get_post_types()'
			),
			'custom-order-heading' => array(
				'name' => 'custom-order-heading',
				'type' => 'heading',
				'label' => 'Custom order'
			),
			'custom-order' => array(
				'type' => 'checkbox',
				'name' => 'custom-order',
				'label' => 'Custom order ....',
				'default' => false,
				'tooltip' => 'Enable Custom-order ... or',
				'toggle'    => array(
					'true' => array(
						'show' => array(
							'#input-order-by',
							'#input-order'
						)
					),
					'false' => array(
						'hide' => array(
							'#input-order-by',
							'#input-order'
						)
					)
				)
			),

		
			'order-by' => array(
				'type' => 'select',
				'name' => 'order-by',
				'label' => 'Order By',
				'tooltip' => '',
				'options' => array(
					'date' => 'Date',
					'title' => 'Title',
					'rand' => 'Random',
					'comment_count' => 'Comment Count',
					'ID' => 'ID'
				)
			),
			

		),

		



		'text' => array(
			'show-titles' => array(
				'type' => 'checkbox',
				'name' => 'show-titles', 
				'label' => 'Show Titles',
				'default' => true
			),

			'content-to-show' => array(
				'type' => 'select',
				'name' => 'content-to-show', 
				'label' => 'Content To Show',
				'options' => array(
					'' => '&ndash; Do Not Show Content &ndash;',
					'excerpt' => 'Excerpts',
					'content' => 'Full Content'
				),
				'default' => 'excerpt',
				'tooltip' => 'The content is the written text or HTML for the entry.  This is edited in the WordPress admin panel. '
			),


		),

		'images' => array(
			// 'crop-vertically' => array(
			// 	'type' => 'checkbox',
			// 	'name' => 'crop-vertically', 
			// 	'label' => 'Crop Vertically',
			// 	'default' => false,
			// 	'tooltip' => 'Trim all images to have the same height.  The trimmed/cropped height is roughly 75% of the width. '
			// )
		),

			);


	function get_categories() {
		
		$category_options = array();
		
		$categories_select_query = get_categories();
		
		foreach ($categories_select_query as $category)
			$category_options[$category->term_id] = $category->name;

		return $category_options;
		
	}
	
	
	function get_authors() {
		
		$author_options = array();
		
		$authors = get_users(array(
			'orderby' => 'post_count',
			'order' => 'desc',
			'who' => 'authors'
		));
		
		foreach ( $authors as $author )
			$author_options[$author->ID] = $author->display_name;
			
		return $author_options;
		
	}
	
	
	function get_post_types() {
		
		$post_type_options = array();

		$post_types = get_post_types(false, 'objects'); 
			
		foreach($post_types as $post_type_id => $post_type){
			
			//Make sure the post type is not an excluded post type. 
			if(in_array($post_type_id, array('revision', 'nav_menu_item'))) 
				continue;
			
			$post_type_options[$post_type_id] = $post_type->labels->name;
		
		}
		
		return $post_type_options;
		
	}
	
	
}