<?php

class headway_accordionBlock extends HeadwayBlockAPI {

    public $id = 'headway_accordion';
    public $name = 'Headway Accordion';
    public $options_class = 'headway_accordionBlockOptions';
    public $description = 'Hedway Accordion';

    
	function enqueue_action($block_id) {

		/* CSS */
		wp_enqueue_style('headway-easyResponsive-css', plugin_dir_url(__FILE__) . '/css/easy-responsive-tabs.css');		

		/* JS */
		wp_enqueue_script('headway-easyResponsive-js', plugin_dir_url(__FILE__) . '/js/easyResponsiveTabs.js', array('jquery'));		

	}


  // // Link na archiv post type
 public function get_archive_link( $post_type ) {
    global $wp_post_types;
    $archive_link = false;
    if (isset($wp_post_types[$post_type])) {
      $wp_post_type = $wp_post_types[$post_type];
      if ($wp_post_type->publicly_queryable)
        if ($wp_post_type->has_archive && $wp_post_type->has_archive!==true)
          $slug = $wp_post_type->has_archive;
        else if (isset($wp_post_type->rewrite['slug']))
          $slug = $wp_post_type->rewrite['slug'];
        else
          $slug = $post_type;
      $archive_link = get_option( 'siteurl' ) . "/{$slug}/";
    }
    return apply_filters( 'archive_link', $archive_link, $post_type );
  }

	    public function setup_elements() {

  ///
      $this->register_block_element(array(
            'id' => 'resp-tabs-container',
            'name' => 'Tabs Container',
            'selector' => '.resp-tabs-container',
            //'properties' => array('property1', 'property2', 'property3'),
            'states' => array(
                'Hover' => '.resp-tabs-container:hover',
                )
            ));

      $this->register_block_element(array(
            'id' => 'accordion-title',
            'name' => 'Title',
            'selector' => '.accordion-title',
            //'properties' => array('property1', 'property2', 'property3'),
            'states' => array(
                'Hover' => '.accordion-title:hover',
                )
      ));
      

      $this->register_block_element(array(
            'id' => 'resp-accordion',
            'name' => 'Tab',
            'selector' => '.resp-accordion',
            //'properties' => array('property1', 'property2', 'property3'),
            'states' => array(
                'Hover' => '.resp-accordion:hover',
                )
            ));
      $this->register_block_element(array(
            'id' => 'resp-tab-active',
            'name' => 'Tab Active',
            'selector' => '.resp-tab-active',
            //'properties' => array('property1', 'property2', 'property3'),
            'states' => array(
                'Hover' => '.resp-tab-active:hover',
                )
            ));

      $this->register_block_element(array(
            'id' => 'resp-tab-content',
            'name' => 'Content',
            'selector' => '.resp-tab-content',
            //'properties' => array('property1', 'property2', 'property3'),
            'states' => array(
                'Hover' => '.resp-tab-content:hover',
                )
            ));


    }


  ////
	// public static function init_action($block_id, $block) 
    // {

    // }


    // public static function enqueue_action($block_id, $block, $original_block = null)
    // {

    // }


    // public static function dynamic_css($block_id, $block, $original_block = null)
    // {

    // }


	// function dynamic_js($block_id, $block = false) {
	// 
	// 	if ( !$block )
	// 		$block = HeadwayBlocksData::get_block($block_id);
	// 
	// 	$js = "
	// 	jQuery(document).ready(function() {
	// 		
	// 	});
	// 	";
	// 
	// 	return $js;
	// 
	// }


public function content($block) {
  /* CODE HERE */

    $title = parent::get_setting($block, 'title');
    $linked = parent::get_setting($block, 'title-link', false);
    $html_tag = parent::get_setting($block, 'title-html-tag', 'h3');


/* Setup Query */
	$query_args = array();

	/* Pagination */
		$paged_var = get_query_var('paged') ? get_query_var('paged') : get_query_var('page');

		if ( (parent::get_setting($block, 'paginate', true) || parent::get_setting($block, 'infinite-scroll', true)) && (headway_get('featured-posts-page') || $paged_var) )
			$query_args['paged'] = headway_get('featured-posts-page') ? headway_get('featured-posts-page') : $paged_var;

	/* Categories */
		if ( parent::get_setting($block, 'categories-mode', 'include') == 'include' ) 
			$query_args['category__in'] = parent::get_setting($block, 'categories', array());

		if ( parent::get_setting($block, 'categories-mode', 'include') == 'exclude' ) 
			$query_args['category__not_in'] = parent::get_setting($block, 'categories', array());	

	$query_args['post_type'] = parent::get_setting($block, 'post-type', false);

	/* FeaturedPosts limit */
		$query_args['posts_per_page'] = parent::get_setting($block, 'total', 3);

	/* Author Filter */
		if ( is_array(parent::get_setting($block, 'author')) )
			$query_args['author'] = trim(implode(',', parent::get_setting($block, 'author')), ', ');
		
	/* Order */
		$query_args['orderby'] = parent::get_setting($block, 'order-by', 'date');
		$query_args['order'] = parent::get_setting($block, 'order', 'DESC');

	/* Status */
		$query_args['post_status'] = 'publish';

	/* Query! */
		$posts = new WP_Query($query_args);

		global $paged; /* Set paged to the proper number because WordPress pagination SUCKS!  ANGER! */
		$paged = $paged_var;
/* End Query Setup */
?>
 <!--Horizontal Tab-->
 <?php if(!empty($posts)):  ?>

<?php if($title){ ?>
          <div class="accordion-title">
            <?php
            if($linked){
              printf('<%s><a href="%s">%s</a></%s>', $html_tag, self::get_archive_link($post_type), $title, $html_tag);
            } else {
              printf('<%s>%s</%s>', $html_tag, $title, $html_tag);
            }
            ?>
          </div>
        <?php } ?>

        <div id="horizontalTab">
            <ul class="resp-tabs-list" data-panelwrapper="panelWrapper1">
            <?php while ( $posts->have_posts() ) : $posts->the_post(); ?>
                <li><?php the_title(); ?></li>
            <?php endwhile; ?>
            </ul>
            <div class="resp-tabs-container">
            <?php while ( $posts->have_posts() ) : $posts->the_post(); ?>
                <div>
                   <?php the_content(); ?>
                </div>
            <?php endwhile; ?>
            </div>
        </div>
        <script type="text/javascript">
    	jQuery(document).ready(function () {
	        jQuery('#horizontalTab').easyResponsiveTabs({
	            type: 'accordion', //Types: default, vertical, accordion           
	            width: 'auto', //auto or any width like 600px
	            fit: true,   // 100% fit in a container
	          //  closed: 'false', // Start closed if in accordion view
	        });
    	});

// For a full explanation of this code, please refer to the blogpost at 
//http://www.bram.us/2014/01/05/css-animated-content-switching/

jQuery(function($) {
  
  $('.resp-tabs-list').each(function(i) {
  
    var $panelNav = $(this),
        $panelNavItems = $panelNav.find('li a'),
        $panelWrapper = $('#' + $panelNav.data('panelwrapper')),
        $panels = $panelWrapper.find('> .panel'),
        enter = $panelWrapper.data('enter'),
        exit = $panelWrapper.data('exit');
    
    // When clicking on any of the panel navigation items
    $panelNavItems.on('click', function(e) {
      
      // Don't follow the link
      e.preventDefault();
      
      // Don't do anything if we are currently animating
      if ($panelWrapper.is('.animating')) return false;
      
      // Don't do anything if the new panel is the current panel
      if ($panels.filter('.current').attr('id') == $($(this).attr('href')).attr('id')) return;
      
      // Get the current active panel
      $panels.filter('.current')
      
        // Slide it out of view
        .addClass('exit ' + exit)
        .removeClass('current');
      
      // Get the panel to slide in
      $panels.filter($(this).attr('href'))
      
        // Slide it into view
        .addClass('enter ' + enter)
      
        // When sliding in is done, 
        .one('transitionend mozTransitionEnd webkitTransitionEnd MSTransitionEnd', function(e) {
          
          // Fix for browsers who fire this handler for both prefixed and unprefixed events (looking at you, Chrome): remove any listeners
          $(this).off('transitionend mozTransitionEnd webkitTransitionEnd  MSTransitionEnd');
          
          // set slid in panel as the current one
          $(this).removeClass().addClass('panel current');

          // reset all other panels
          $panels.filter(':not(#' + this.id  + ')').removeClass().addClass('panel');
          
          // Allow a new animation
          $panelWrapper.removeClass('animating');

      });
                   
      // Animate!
      // @note: using a setTimeout because "it solves everything", dixit @rem
      setTimeout(function() {
       $panelWrapper.addClass('animating');
      }, 0);
      
    });
    
  });
  
});
		</script>
<?php endif; ?>
<?php
		
		
    }
}