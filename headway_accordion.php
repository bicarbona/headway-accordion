<?php
/**
 * Plugin Name: Headway Accordion
 * Plugin URI:  https://bitbucket.org/bicarbona/headway-accordion
 * Description: Headway Accordion
 * Version:     0.1.5
 * Author:      Bicarbona
 * Author URI:  
 * License:     GPLv2+
 * Text Domain: headway_accordion
 * Domain Path: /languages
 	Domain Path: /languages
 	Bitbucket Plugin URI: https://bitbucket.org/bicarbona/headway-accordion
 	Bitbucket Branch: master
*/

/**
 * Everything is ran at the after_setup_theme action to insure that all of Headway's classes and functions are loaded.
 **/
add_action('after_setup_theme', 'headway_accordion_register');
function headway_accordion_register() {

	if ( !class_exists('Headway') )
		return;

	require_once 'includes/Block.php';
	require_once 'includes/BlockOptions.php';
	//require_once 'design-editor-settings.php';

	return headway_register_block('headway_accordionBlock', plugins_url(false, __FILE__));
}

/**
 * Prevent 404ing from breaking Infinite Scrolling
 **/
add_action('status_header', 'headway_accordion_prevent_404');
function headway_accordion_prevent_404($status) {

	if ( strpos($status, '404') && get_query_var('paged') && headway_get('pb') )
		return 'HTTP/1.1 200 OK';

	return $status;
}

/**
 * Prevent WordPress redirect from messing up featured board pagination
 */
add_filter('redirect_canonical', 'headway_accordion_redirect');
function headway_accordion_redirect($redirect_url) {

	if ( headway_get('pb') )
		return false;

	return $redirect_url;
}